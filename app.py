from flask import Flask

app = Flask(__name__)


def capitalize(word: str) -> str:
    return word.title()


@app.route('/')
def home():
    return "Welcome! Check out /greeter/<your name> too!"


@app.route('/capitalize/<word>')
def capitalize_request(word: str) -> str:
    return capitalize(word)


@app.route('/greeter/<name>')
def greeter_request(name: str) -> str:
    return f'Helloooo {capitalize(name)}!!!!!'

if __name__ == '__main__':
    app.run(host='0.0.0.0')
